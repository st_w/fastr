package com.oracle.truffle.r.nodes.builtin.rawi;

public class RawHelpers {

    static int bswap32(int x) {
        return (x & 0xFF) << 24 | (x & 0xFF00) << 8 | (x & 0xFF0000) >> 8 | (x >> 24) & 0xFF;
    }

    static int rawShift32LE(int x, int n) {
        if (n == 0) {
            return x;
        } else if (n < 0) {
            return x >>> ((-n) % 32);
        } else {
            return x << (n % 32);
        }
    }

    static int rawRoL32LE(int x, int n) {
        if (n == 0) {
            return x;
        }
        if (n < 0) {
            // (32 - ((-n) % 32))
            return (x >>> ((-n) % 32)) | (x << (32 + (n % 32)));
        } else {
            return (x << (n % 32)) | (x >>> (32 - (n % 32)));
        }
    }
}
