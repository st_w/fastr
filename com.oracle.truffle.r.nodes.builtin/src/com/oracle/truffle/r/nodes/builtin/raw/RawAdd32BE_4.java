/*
 * Copyright (c) 2013, 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.builtin.raw;

import static com.oracle.truffle.r.runtime.builtins.RBehavior.PURE;
import static com.oracle.truffle.r.runtime.builtins.RBuiltinKind.PRIMITIVE;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.r.nodes.builtin.RBuiltinNode;
import com.oracle.truffle.r.nodes.builtin.NodeWithArgumentCasts.Casts;
import com.oracle.truffle.r.runtime.builtins.RBuiltin;
import com.oracle.truffle.r.runtime.data.RDataFactory;
import com.oracle.truffle.r.runtime.data.RRawVector;

@RBuiltin(name = ".fastr.raw.RawAdd32BE_4", kind = PRIMITIVE, parameterNames = {"x1", "x2", "x3", "x4"}, behavior = PURE)
public abstract class RawAdd32BE_4 extends RBuiltinNode {

    static {
        Casts casts = new Casts(RawAdd32BE_4.class);
        casts.arg("x1").asRawVector();
        casts.arg("x2").asRawVector();
        casts.arg("x3").asRawVector();
        casts.arg("x4").asRawVector();
    }

    @Specialization
    protected RRawVector rawAdd32BE_4(RRawVector x1, RRawVector x2, RRawVector x3, RRawVector x4) {
        byte[] data = RawHelpers.intToRawBE(RawHelpers.rawToIntBE(x1) + RawHelpers.rawToIntBE(x2) + RawHelpers.rawToIntBE(x3) + RawHelpers.rawToIntBE(x4));
        return RDataFactory.createRawVector(data);
    }

}
