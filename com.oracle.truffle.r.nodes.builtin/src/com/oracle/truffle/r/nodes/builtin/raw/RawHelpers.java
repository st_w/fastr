package com.oracle.truffle.r.nodes.builtin.raw;

import com.oracle.truffle.r.runtime.data.RRawVector;

public class RawHelpers {

    static int rawToIntLE(RRawVector x) {
        byte[] data = x.getDataWithoutCopying();
        return rawToIntLE(data);
    }

    static int rawToIntBE(RRawVector x) {
        byte[] data = x.getDataWithoutCopying();
        return rawToIntBE(data);
    }

    static int rawToIntLE(byte[] x) {
        assert x.length == 4;
        return x[3] & 0xFF | (x[2] << 8) & 0xFF00 | (x[1] << 16) & 0xFF0000 | (x[0] << 24) & 0xFF000000;
    }

    static int rawToIntBE(byte[] x) {
        assert x.length == 4;
        return x[0] & 0xFF | (x[1] << 8) & 0xFF00 | (x[2] << 16) & 0xFF0000 | (x[3] << 24) & 0xFF000000;
    }

    static byte[] intToRawLE(int x) {
        byte[] data = new byte[4];
        data[3] = (byte) (x & 0xFF);
        data[2] = (byte) ((x >>> 8) & 0xFF);
        data[1] = (byte) ((x >>> 16) & 0xFF);
        data[0] = (byte) ((x >>> 24) & 0xFF);
        return data;
    }

    static byte[] intToRawBE(int x) {
        byte[] data = new byte[4];
        data[0] = (byte) (x & 0xFF);
        data[1] = (byte) ((x >>> 8) & 0xFF);
        data[2] = (byte) ((x >>> 16) & 0xFF);
        data[3] = (byte) ((x >>> 24) & 0xFF);
        return data;
    }

    static byte[] rawShift32BE(byte[] x, int n) {
        if (n == 0) {
            return x;
        } else if (n < 0) {
            return intToRawBE(rawToIntBE(x) >>> ((-n) % 32));
        } else {
            return intToRawBE(rawToIntBE(x) << (n % 32));
        }
    }

    static byte[] rawShift32LE(byte[] x, int n) {
        if (n == 0) {
            return x;
        } else if (n < 0) {
            return intToRawLE(rawToIntLE(x) >>> ((-n) % 32));
        } else {
            return intToRawLE(rawToIntLE(x) << (n % 32));
        }
    }

    static byte[] rawRoL32BE(byte[] x, int n) {
        if (n == 0) {
            return x;
        }
        int v = rawToIntBE(x);
        if (n < 0) {
            return intToRawBE((v >>> ((-n) % 32)) | (v << (32 + (n % 32))));
        } else {
            return intToRawBE((v << (n % 32)) | (v >>> (32 - (n % 32))));
        }
    }

    static byte[] rawRoL32LE(byte[] x, int n) {
        if (n == 0) {
            return x;
        }
        int v = rawToIntLE(x);
        if (n < 0) {
            // (32 - ((-n) % 32))
            return intToRawLE((v >>> ((-n) % 32)) | (v << (32 + (n % 32))));
        } else {
            return intToRawLE((v << (n % 32)) | (v >>> (32 - (n % 32))));
        }
    }
}
