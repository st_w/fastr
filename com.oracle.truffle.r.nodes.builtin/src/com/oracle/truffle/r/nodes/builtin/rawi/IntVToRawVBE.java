/*
 * Copyright (c) 2013, 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.builtin.rawi;

import static com.oracle.truffle.r.runtime.builtins.RBehavior.PURE;
import static com.oracle.truffle.r.runtime.builtins.RBuiltinKind.PRIMITIVE;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.r.nodes.builtin.RBuiltinNode;
import com.oracle.truffle.r.nodes.builtin.NodeWithArgumentCasts.Casts;
import com.oracle.truffle.r.nodes.builtin.raw.RawToIntBE;
import com.oracle.truffle.r.runtime.builtins.RBuiltin;
import com.oracle.truffle.r.runtime.data.RDataFactory;
import com.oracle.truffle.r.runtime.data.RIntVector;
import com.oracle.truffle.r.runtime.data.RNull;
import com.oracle.truffle.r.runtime.data.RRawVector;

@RBuiltin(name = "fastr.rawi.intVToRawVBE", kind = PRIMITIVE, parameterNames = {"x"}, behavior = PURE)
public abstract class IntVToRawVBE extends RBuiltinNode {

    static {
        Casts casts = new Casts(IntVToRawVBE.class);
        casts.arg("x").asIntegerVector();
    }

    private static RRawVector intVToRawVBE(int[] xd) {
        byte[] data = new byte[xd.length * 4];
        int i = 0;
        for (int q = 0; q < xd.length; q++) {
            int t = xd[q];
            data[i++] = (byte) (t & 0xFF);
            data[i++] = (byte) ((t >>> 8) & 0xFF);
            data[i++] = (byte) ((t >>> 16) & 0xFF);
            data[i++] = (byte) ((t >>> 24) & 0xFF);
        }
        return RDataFactory.createRawVector(data);
    }

    @Specialization
    protected RRawVector intVToRawVBE(RIntVector x) {
        return intVToRawVBE(x.getDataWithoutCopying());
    }

    @Specialization
    protected RRawVector intVToRawVBE(Integer x) {
        return intVToRawVBE(new int[]{(x)});
    }

    @Specialization
    protected RRawVector intVToRawVBE(@SuppressWarnings("unused") RNull x) {
        return RDataFactory.createEmptyRawVector();
    }

}
