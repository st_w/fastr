/*
 * Copyright (c) 2013, 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.builtin.rawi;

import static com.oracle.truffle.r.runtime.builtins.RBehavior.PURE;
import static com.oracle.truffle.r.runtime.builtins.RBuiltinKind.PRIMITIVE;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.r.nodes.builtin.RBuiltinNode;
import com.oracle.truffle.r.nodes.builtin.NodeWithArgumentCasts.Casts;
import com.oracle.truffle.r.runtime.builtins.RBuiltin;
import com.oracle.truffle.r.runtime.data.RDataFactory;
import com.oracle.truffle.r.runtime.data.RIntVector;
import com.oracle.truffle.r.runtime.data.RNull;
import com.oracle.truffle.r.runtime.data.RRaw;
import com.oracle.truffle.r.runtime.data.RRawVector;

@RBuiltin(name = "fastr.rawi.rawVToIntVLE", kind = PRIMITIVE, parameterNames = {"x"}, behavior = PURE)
public abstract class RawVToIntVLE extends RBuiltinNode {

    static {
        Casts casts = new Casts(RawVToIntVLE.class);
        casts.arg("x").asRawVector();
    }

    private static RIntVector rawVToIntVLE(byte[] xd) {
        int[] data = new int[(xd.length + 3) / 4];
        int i = 0;
        for (int q = 0; q < data.length; q++) {
            int t = 0;
            if (q >= xd.length / 4) {
                if (i < xd.length) {
                    t |= (xd[i++] << 24) & 0xFF000000;
                }
                if (i < xd.length) {
                    t |= (xd[i++] << 16) & 0xFF0000;
                }
                if (i < xd.length) {
                    t |= (xd[i++] << 8) & 0xFF00;
                }
                if (i < xd.length) {
                    t |= xd[i++] & 0xFF;
                }
            } else {
                t = xd[i + 3] & 0xFF | (xd[i + 2] << 8) & 0xFF00 | (xd[i + 1] << 16) & 0xFF0000 | (xd[i] << 24) & 0xFF000000;
                i += 4;
            }
            data[q] = t;
        }
        // "complete" means: does not contain NAs
        return RDataFactory.createIntVector(data, false);
    }

    @Specialization
    protected RIntVector rawVToIntVLE(@SuppressWarnings("unused") RNull x) {
        return RDataFactory.createEmptyIntVector();
    }

    @Specialization
    protected RIntVector rawVToIntVLE(RRawVector x) {
        return rawVToIntVLE(x.getDataWithoutCopying());
    }

    @Specialization
    protected RIntVector rawVToIntVLE(RRaw x) {
        return rawVToIntVLE(new byte[]{x.getRawDataAt(0)});
    }

}
