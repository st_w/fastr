/*
 * Copyright (c) 2013, 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.builtin.rawi;

import static com.oracle.truffle.r.runtime.builtins.RBehavior.PURE;
import static com.oracle.truffle.r.runtime.builtins.RBuiltinKind.PRIMITIVE;

import java.security.InvalidParameterException;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.r.nodes.builtin.CastBuilder;
import com.oracle.truffle.r.nodes.builtin.RBuiltinNode;
import com.oracle.truffle.r.nodes.builtin.NodeWithArgumentCasts.Casts;
import com.oracle.truffle.r.runtime.builtins.RBuiltin;
import com.oracle.truffle.r.runtime.data.RDataFactory;
import com.oracle.truffle.r.runtime.data.RDouble;
import com.oracle.truffle.r.runtime.data.RIntVector;
import com.oracle.truffle.r.runtime.data.RInteger;
import com.oracle.truffle.r.runtime.data.RRawVector;
import com.oracle.truffle.r.runtime.data.model.RAbstractVector;

@RBuiltin(name = "fastr.rawi.numToIntBE", kind = PRIMITIVE, parameterNames = {"x"}, behavior = PURE)
public abstract class NumToIntBE extends RBuiltinNode {

    static {
        Casts casts = new Casts(NumToIntBE.class);
        casts.arg("x").mustBe(CastBuilder.Predef.numericValue()).asDoubleVector();
    }

    @Specialization
    protected Integer numToIntBE(Double x) {
        long l = x.longValue();
        return RawHelpers.bswap32((int) (l & 0xFFFFFFFF));
    }

    @Specialization
    protected Integer numToIntBE(Integer x) {
        return RawHelpers.bswap32(x);
    }

    @Specialization
    protected RIntVector numToIntBE(RAbstractVector x) {
        RIntVector r = RDataFactory.createIntVector(x.getLength());
        int[] rd = r.getInternalStore();
        for (int q = 0; q < x.getLength(); q++) {
            Object d = x.getDataAtAsObject(q);
            if (d instanceof Integer) {
                rd[q] = RawHelpers.bswap32((Integer) d);
            } else if (d instanceof Double) {
                rd[q] = RawHelpers.bswap32((int) (((Double) d).longValue() & 0xFFFFFFFF));
            } else {
                throw new InvalidParameterException("Invalid vector type: " + d.getClass().toString());
            }
        }
        return r;
    }

}
